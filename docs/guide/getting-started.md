---
title: Getting Started
author: Lester Bolotaolo
lang: en-US
description: Getting started with the IPMS
date: 2020-06-02 17:04:00 +8
tags: 
  - login
  - password
  - forgot password
  - reset password
  - verify email
  - create account
  - sign up
---

# Getting Started with the IPMS

This section of the manual aims to inform the user of how to create an account, login, verify email, and recover forgotten password. The content are as follows:

[[TOC]]

## Welcome Screen

When you navigate to the application's URL, you will be shown the welcome screen of the IPMS. At the top-right, there are two buttons to help you navigate: `Getting Started` and `Login`. Clicking on `Getting Started` will take you to the system's documentation and tutorial/help. `Login`, on the other hand, will take you to the login page. 

![landing_page](https://user-images.githubusercontent.com/29625844/83469518-87efe000-a4b2-11ea-9f05-625b4ab080a9.png)

## Creating a new account

To use the application, a user needs to create an account.

1. Go to the Login page by typing `https://da-ipms2020.web.app/login` or by clicking the `Login` button in the welcome screen.

2. Once in the Login page, click on the `Sign up` at the bottom. The screen below should appear.

![signup_page](https://user-images.githubusercontent.com/29625844/83469231-adc8b500-a4b1-11ea-81af-f7ded80b2214.png)

3. To sign up, just provide your `full name`(this will help admin identify you), your email (any email will do but it has to be valid), your `password` (must be at least 8 characters), and retype the password to confirm. You can use the `visibility icon` to peek at your password.

4. Finally, click on the `Sign Up` button.

The signup process may take a few seconds but if it doesn't complete, something must be wrong. Otherwise, you should see the following screen:

![signup_successful](https://user-images.githubusercontent.com/29625844/83471739-0e5af080-a4b8-11ea-86c5-30bacc7d88fc.png)

As with other application, while not required, make sure you verify your [email](#verify-email). Instructions are shown below.

## Verifying your email address

Upon creating a new account, you will receive an email from the system asking you to verify your email. Please check your `SPAM` folder as verification email often enters spam folder. The email should look like the picture below:

![verify_email](https://user-images.githubusercontent.com/29625844/83473313-d6ee4300-a4bb-11ea-96cd-3591f76c62fe.png)

Just click on the `Verify Email` button. You should see the following screen:

![verify_success](https://user-images.githubusercontent.com/29625844/83473052-43b50d80-a4bb-11ea-9078-2a5593091643.png)

**Verifying your email address ensures strengthened security, allows IPD staff to better assist you if you forget your password, and gives you access to more features on IPMS.**

## Login to your account

Note that as soon as you create an account, you can already login. Email verification ensures you can recover your account in case you forget/lose your password. To login, go to the login page and enter the email and password you used to create your account. Then, click `Login`.

![login_page](https://user-images.githubusercontent.com/29625844/83469362-10ba4c00-a4b2-11ea-812d-305e5aa3641c.png)

## Recovering Forgotten Password

If you forget your password, just click on the `Forgot Password` and enter the email you used to create your account. This will send a Password Reset link to your email. Remember to check your `SPAM` folder. Note that you cannot recover your password since this is encrypted. Instead, you will need to reset your password through the link sent to your email. This is why having a verified email is very important. The password reset link is valid for 60 minutes.

![forgot_password](https://user-images.githubusercontent.com/29625844/83469416-39dadc80-a4b2-11ea-8ac8-fde7c4755c4b.png)

Upon clicking the `Reset Password`, you will be prompted to confirm visiting a `Suspicious Link`. You may ignore this warning and click `Confirm.`

![reset_password](https://user-images.githubusercontent.com/29625844/83474529-d60ae080-a4be-11ea-8f7d-4232b54ff963.png)

You will be taken to the screen where you can create a new password. This is referenced in [#44](https://github.com/mlab817/q-pipol/issues/44#issue-628861813) for improvement.

![reset_password_screen](https://user-images.githubusercontent.com/29625844/83474789-7103ba80-a4bf-11ea-815c-660f05219365.png)

If you encounter no problems, you will see the following problem. Sometimes, the System will prompt you that the token is invalid. In this case, just send another `Forgot Password` request from the `Login` page.

![password_reset_success](https://user-images.githubusercontent.com/29625844/83475115-3fd7ba00-a4c0-11ea-940f-04169ede2216.png)

## Next Steps

- [Dashboard](/guide/dashboard)
- [Projects]()
- [Profile]()
- [Settings]()

<TagLinks />
