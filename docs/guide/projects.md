---
title: Project Manager
tags: 
  - add project
  - update project
  - delete project
  - view project
  - transfer project
  - endorse project
  - review project
  - validate project
  - endorse project
  - monitor project
  - share project
  - search project
---

# Projects

[[toc]]

## View Projects

This shows the list of projects accessible to the users. Note that currently, projects are filtered based on user inputs (projects they own), user affiliation (projects belonging to their office) and user assigned operating units (operating units they can view/review).

![projects](https://user-images.githubusercontent.com/29625844/83590122-9a881900-a587-11ea-9226-2db2a495c675.png)

The project menu has five options:

- View - view more details of the project
- Update - update the details of the project
- Review - review the project
- Add - add projects to list for endorsement
- Delete - delete the project

Rather than hiding options not accessible to users, they are shown as disabled.

![project menu](https://user-images.githubusercontent.com/29625844/83590177-b8ee1480-a587-11ea-8ea5-3cdcd6e6be7e.png)

## Project Submission Flow

The figure below show the general project submission flow. It starts with encoder [adding a project](#add-project). After adding a project, the encoder is expected to [update it](#update-project) and complete the required information. Once the required information is complete, they need to [finalize it](#finalize-project). Only finalized project can be [endorsed](#endorse-project). Once endorsed, the `reviewer` validates the endorsement (whether the project is included in the endorsement letter or not) and the completeness of information. If found compliant, the `reviewer` can review the project; otherwise, they will return it for compliance. After review is final, the `lead` will review if the review is okay and [accept it](#accept-project). Finally, the IPD Chief will [approve](#approve-project) the inclusion of the project in the system.  

![project submission flow](https://user-images.githubusercontent.com/29625844/83707848-97576080-a64d-11ea-90e8-31c98856bc30.png)

## Add Project

Add project feature is available to users with `ENCODER` role only.

## View Project

You can view the details of the project from the list of projects by clicking on the dropdown and selecting `View Project`. Security on view project is not as strict as other functions. As long as you can view it in the list of projects, you can view its details.

![View Project](https://user-images.githubusercontent.com/29625844/83824911-5bce9c00-a70a-11ea-8921-37cbb94b3ae9.png)

## Update Project

This function is exclusive to users with the `Encoder` role and it can only be done to projects which were not finalized and/or were returned.

## Finalize Project

## Endorse Project

## Validate Project

## Review Project

## Accept Project

## Approve Project

# Miscellaneous Functions

## Transfer Project

## Delete Project

## Monitor Project

## Share Project (enhancement)

## Search Project

![search project](https://user-images.githubusercontent.com/29625844/83590235-dfac4b00-a587-11ea-8f5f-7b64828e9b6f.png)