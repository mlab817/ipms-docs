---
title: API Reference
author: Lester Bolotaolo
---

# API

This is a guide on using the IPMS API. All requests are coursed through the `${URL}/graphql` endpoint.